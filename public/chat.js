$(function() {
  var socket = io();

  socket.on("chat message connect", function(text) {
    $("#user__ifo").text(text);
  });

  $("form[name=form]").submit(function(e) {
    e.preventDefault();

    socket.emit("chat message", $("#m").val());
    $("#m").val("");

    return false;
  });

  socket.on("chat message", function(msg) {
    $("#messages").append($("<li>").text(msg));
  });

  socket.on("chat message disconnect", function(text) {
    $("#user__ifo").text(text);
  });
});
