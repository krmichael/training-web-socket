module.exports = {
  dir: "../tmp",
  stringify: JSON.stringify,
  parse: JSON.parse,
  encoding: "utf8"
};
