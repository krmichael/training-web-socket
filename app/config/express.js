const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const path = require("path");

module.exports = function() {
  app.use(bodyParser.urlencoded({ extended: false }));
  app.set("view engine", "ejs");
  app.use(express.static(path.resolve(__dirname, "..", "..", "public")));

  return app;
};
