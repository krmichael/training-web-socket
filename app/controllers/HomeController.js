const storage = require("node-persist");
const configStorage = require("../config/persist");

module.exports = {
  index(req, res) {
    return res.render("home");
  },

  async store(req, res) {
    const { username } = req.body;

    await storage.init(configStorage);
    await storage.setItem("@chat:username", username);

    return res.redirect("/chat");
  }
};
