const express = require("express");
const routes = express.Router();
const HomeController = require("./controllers/HomeController");
const ChatController = require("./controllers/ChatController");

routes.get("/", HomeController.index);
routes.post("/", HomeController.store);

routes.get("/chat", ChatController.index);

module.exports = routes;
