const express = require("./app/config/express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);

const configStorage = require("./app/config/persist");
const storage = require("node-persist");

app.use("/", require("./app/routes"));

io.on("connection", async socket => {
  await storage.init(configStorage);
  const username = await storage.getItem("@chat:username");

  io.emit("chat message connect", `user ${username} connected`);

  socket.on("chat message", msg => {
    io.emit("chat message", `${username}: ${msg}`);
  });

  socket.on("disconnect", () => {
    io.emit("chat message disconnect", `user ${username} disconnected`);
  });
});

http.listen(3000, () => {
  console.log("Server running at http://localhost:3000");
});
